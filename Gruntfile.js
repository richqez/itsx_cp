module.exports = function (grunt) {
  grunt.initConfig({
    watch: {
      css: {
        files: ['**/*.css'],
        options: {
          livereload: true
        }
      },
      html: {
        files: ['*.html'],
        options: {
          livereload: true
        }
      },
      template: {
        files: ['template/*.html'],
        options: {
          livereload: true
        }
      },
      js: {
        files: ['**/*.js'],
        options: {
          livereload: true
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch'])
  
};