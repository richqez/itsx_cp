angular.module('itsx_cp.controllers',[])

	.controller('SigninCtrl', ['$scope','AuthSv','$localStorage','$location','UserSv', function ($scope,AuthSv,$localStorage,$location,UserSv) {
		
		$scope.isError = false ;

		$scope.login = function(){
			AuthSv.signin($scope.user,
				function(res){
					$localStorage.token = res.token;
					var userToken = AuthSv.getDecodeToken(res.token);
					UserSv.getUser(userToken.sub,
	         			function(res){
		         			UserSv.currentUser.set(res);
		         			$localStorage.currentUser = res ;
	         				window.location = "../itsx_cp/#/shospital";
	         			},
	         			function(res){

         				}
         			);
				},
				function(res){
					$scope.isError = true ;
				});
		}	
	}])
	.controller('DashCtrl', ['$scope','AuthSv', function ($scope,AuthSv) {

		  

	
	}])
	.controller('UserStatCtrl', ['$scope','StatSv', function($scope,StatSv){

		$scope.getUserStat = function(){
			StatSv.getUserStat(function(res){
				$scope.labels = res.labels;
				$scope.data =res.data ;
				$scope.series = ['USER IN SYSTEMS'];
			},
			function(){

			});
		}

		$scope.getUserStat()
	}])
	.controller('EventStatCtrl', ['$scope','StatSv',function ($scope,StatSv) {
		
		$scope.getEventStat = function(){
			StatSv.getEventStat(function(res){
				$scope.labels = res.labels;
				$scope.data = res.data;
				$scope.series = ['EVENT IN SYSTEMS'] ;
			},function(){

			});
		}


		$scope.getEventStat();


	}])
	.controller('NavCtrl', ['$scope','AuthSv','UserSv','$localStorage','$location',function ($scope,AuthSv,UserSv,$localStorage,$location) {
		
		$scope.getCurrentuser = function(){
			if (typeof UserSv.currentUser.get() === 'undefined') {
				if ($localStorage.token) {
					var userToken = AuthSv.getDecodeToken($localStorage.token);
					UserSv.getUser(userToken.sub,
		         		function(res){
			         		UserSv.currentUser.set(res);
			         		$scope.user =res ;
		         		},
		         		function(res){

	         			}
	         		);
				};
			}

		}
		$scope.logout = function () {

               AuthSv.logout(function () {
                   window.location = "/itsx_cp"
               });
           };
        $scope.getCurrentuser();
        $scope.user = UserSv.currentUser.get();
     
	}])
	.controller('EventCtrl', ['$scope','EventSv','$localStorage','StatSv',function ($scope,EventSv,$localStorage,StatSv) {
		
		$scope.getStat =function(){
			StatSv.all(function(res){
				$scope.stat = res ;
			},
			function(){

			});
		}

		$scope.getStat();

	}])
	.controller('EventAllCtrl', ['$scope','EventSv', function ($scope,EventSv) {
		
		$scope.getAll = function(){
			EventSv.findAll(function(res){
				$scope.events = res ;
			},
			function(){

			});
		}

		$scope.getAll();


	}])
	.controller('EventACtrl', ['$scope','EventSv', function ($scope,EventSv) {

		$scope.getA =function(){
			EventSv.findA(function(res){
				$scope.events = res;
			},function(){

			})
		}

		console.log("sdasd");
		$scope.getA();


	}])
	.controller('EventUCtrl', ['$scope','EventSv', function ($scope,EventSv) {

		$scope.getU = function(){
			EventSv.findU(function(res){
				$scope.events = res ;
			},function(){

			});
		}

		$scope.getU();


	}])
	.controller('HospitalCtrl', ['$scope','HospitalSv','AuthSv', function ($scope,HospitalSv,AuthSv) {
		
		$scope.listOfHospital = [];

		$scope.addHospital = function(){

			HospitalSv.add($scope.hospital,
				function(res){
					$scope.getAllHospital();
					$scope.hospital = null;
				},
				function(res){
				});
		}

		$scope.removeHospital = function(id){

			HospitalSv.remove(id,
				function(res){
					$scope.getAllHospital();
				},
				function(){

				});
		}

		$scope.getAllHospital = function(){

			HospitalSv.findAll(
				function(res){
					$scope.listOfHospital = res ;
				},
				function(res){
				});
		}

		$scope.updateHospital = function(data){

			HospitalSv.update(data,
				function(){
				console.log("update");
				$scope.getAllHospital();
				},
				function(){


				});
		}
		$scope.getAllHospital();
	}])
	.controller('ModalHospitalCtrl',['$scope','$uibModal','$log','HospitalSv','NgMap','$timeout',function ($scope, $uibModal, $log,HospitalSv,NgMap,$timeout) {

	  $scope.animationsEnabled = true;

	  $scope.open = function (size,data) {

	    var modalInstance = $uibModal.open({
	      animation: $scope.animationsEnabled,
	      templateUrl: 'template/modal.html',
	      controller: 'ModalHospitalInstanceCtrl',
	      size: size,
	      resolve: {
	        data: function () {
	          return data;
	        }
	      }
	    })

	     modalInstance.result.then(
	  		function (data) {
	  		console.log(data);
	  		$scope.$parent.updateHospital(data);
	  		$scope.$parent.getAllHospital();
	  		}, 
	  		function () {	      

	    	}
	    );
	 }

	 $scope.openMap = function(size,data){

	 	 var modalInstance = $uibModal.open({
	      animation: $scope.animationsEnabled,
	      templateUrl: 'template/modalmap.html',
	      controller: 'ModalMapInstanceCtrl',
	      size: size,
	      resolve: {
	        data: function () {
	          return data;
	        },
	        ngmap:function(){
	        	return NgMap;
	        },
	        $timeout:function(){
	        	return $timeout;
	        }
	      }
	    })

	     modalInstance.result.then(
	  		function (data) {
	  		console.log(data);
	  		}, 
	  		function () {	      

	    	}
	    );

	 }

	  $scope.toggleAnimation = function () {
	    $scope.animationsEnabled = !$scope.animationsEnabled;
	  };
	}])
	.controller('ModalHospitalInstanceCtrl', function ($scope, $uibModalInstance, data) {

	  $scope.hospital = angular.copy(data);

	  $scope.ok = function () {
	    $uibModalInstance.close($scope.hospital);
	  };

	  $scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	    //$scope.hospital = '';
	  };
	})
	.controller('ModalMapInstanceCtrl',function ($scope, $uibModalInstance,data,NgMap,$timeout) {
		
		$scope.hospital = data ;


		NgMap.getMap().then(function(map) {
		    console.log(map.getCenter());
		    console.log('markers', map.markers);
		    console.log('shapes', map.shapes);
		  });

		$scope.render= true;

		$scope.reloadmap = function(){
			$scope.render = false ;
			$timeout(function(){$scope.render = true}, 2000); 
		}

		$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	    //$scope.hospital = '';
	  };

	  $scope.reloadmap();

		
	})
