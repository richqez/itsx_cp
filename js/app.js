'use strict';

angular
	.module('itsx_cp'
		,['ngStorage',
		  'ui.bootstrap',
		  'angular-loading-bar',
		  'ngAnimate',
		  'ui.router',
		  'angularUtils.directives.dirPagination',
		  'chart.js',
		  'ngMap',
		  'itsx_cp.controllers',
		  'itsx_cp.services'
		])
	.run(['$rootScope','$location',function ($rootScope,$location) {
		$rootScope.isActive = function(viewLocation){
				return viewLocation === $location.path();
		}
	}])
	.run(['$rootScope','$state',function ($rootScope, $state) {
		 $rootScope.$state = $state;
	}])
	.constant('urls', {
		BASE: 'http://zzxxccvvbbnn.esy.es/public',
		BASE_API: 'http://zzxxccvvbbnn.esy.es/public/api'
		})
	.config(['paginationTemplateProvider',function (paginationTemplateProvider) {
		paginationTemplateProvider.setPath('template/dirPagination.tpl.html');
	}])
	.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	    //cfpLoadingBarProvider.spinnerTemplate = '<div><span class="fa fa-spinner">Loading...</div>';
	  }])
	.config(['ChartJsProvider',function (ChartJsProvider) {
		  ChartJsProvider.setOptions({
		      colours: [ '#F7464A'],
		      responsive: true
		    });
	}])
	.config(['$httpProvider','$stateProvider','$urlRouterProvider',function ($httpProvider,$stateProvider,$urlRouterProvider) {

		$urlRouterProvider.otherwise("/dash");

		$stateProvider
			.state('signin',{
				url:'/signin',
				templateUrl: 'template/signin.html',
				controller: 'SigninCtrl'
			})
			.state('hospital',{
				url:'/hospital',
				templateUrl : 'template/hospital.html',
				controller: 'HospitalCtrl'
			})
			.state('event',{
				url:'/event',
				templateUrl: 'template/event.html',
				controller: 'EventCtrl'
			})
			.state('event.all',{
				url:'/all',
				templateUrl:'template/event.all.html',
				controller:'EventAllCtrl'
			})
			.state('event.a',{
				url:'/a',
				templateUrl: 'template/event.a.html',
				controller : 'EventACtrl'
			})
			.state('event.u',{
				url:'/b',
				templateUrl: 'template/event.u.html',
				controller: 'EventUCtrl'
			})
			.state('dash',{
				url:'/dash',
				templateUrl:'template/dash.html',
				controller:'DashCtrl'
			});

		$httpProvider.interceptors.push(['$q', '$location', '$localStorage', function ($q, $location, $localStorage) {
			   return {
			       'request': function (config) {
			           config.headers = config.headers || {};
			           if ($localStorage.token) {
			               config.headers.Authorization = 'Bearer ' + $localStorage.token;
			           }
			           return config;
			       },
			       'responseError': function (response) {
			           if (response.status === 401 || response.status === 403 || response.status === 400) {
			               $location.path('/signin');
			           }
			           return $q.reject(response);
			       }
			   }
			}]);
		}])


	