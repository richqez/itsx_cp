angular.module('itsx_cp.services',[])
	
  .factory('AuthSv', ['$http', '$localStorage', 'urls',function ($http, $localStorage, urls) {
		
		function urlBase64Decode(str) {
           var output = str.replace('-', '+').replace('_', '/');
           switch (output.length % 4) {
               case 0:
                   break;
               case 2:
                   output += '==';
                   break;
               case 3:
                   output += '=';
                   break;
               default:
                   throw 'Illegal base64url string!';
           }
           return window.atob(output);
       }
       function getClaimsFromToken() {
           var token = $localStorage.token;
           var user = {};
           if (typeof token !== 'undefined') {
               var encoded = token.split('.')[1];
               user = JSON.parse(urlBase64Decode(encoded));
           }
           return user;
       }

       function getDecodeToken(token) {
           var user = {};
           if (typeof token !== 'undefined') {
               var encoded = token.split('.')[1];
               user = JSON.parse(urlBase64Decode(encoded));
           }
           return user;
       }

        var tokenClaims = getClaimsFromToken();
        
		    return {
           signup: function (data, success, error) {
               $http.post(urls.BASE + '/register', data).success(success).error(error)
           },
           signin: function (data, success, error) {
               $http.post(urls.BASE + '/auth/login', data).success(success).error(error)
           },
           logout: function (success) {
               tokenClaims = {};
               delete $localStorage.token;
               success();
           },
           getTokenClaims: function () {
               return tokenClaims;
           },
           getDecodeToken:function(token){
              return getDecodeToken(token);
           }
       };
	  }])
  .factory('UserSv',['$http','urls',function ($http,urls) {
        var _currentUser ;
        var user = {};
        user.set = function(user){
          _currentUser = user ;
        }
        user.get = function(){
          return _currentUser ;
        }
        return {
            getUser:function(subid,success,error){
              $http.get(urls.BASE_API+'/user/'+subid).success(success).error(error);
            },
            currentUser: user 
        };
    }])
	.factory('HospitalSv', ['$http','urls',function ($http,urls) {
		return {
			add:function(data,success,error){
				$http.post(urls.BASE_API+'/hospital',data).success(success).error(error);
			},
			findAll:function(success,error){
				$http.get(urls.BASE_API+'/hospital').success(success).error(error);
			},
			remove:function(id,success,error){
				$http.delete(urls.BASE_API+'/hospital/'+id).success(success).error(error);
			},
      update:function(data,success,error){
        $http.put(urls.BASE_API+'/hospital/'+data.hospital_id,data).success(success).error(error);
      }
		};
	  }])
  .factory('EventSv', ['$http','urls', function ($http,urls) {
    return {
      add:function(success,error){
        $http.post(urls.BASE_API+'event',data).success(success).error(error);
      },
      findAll:function(success,error){
        $http.get(urls.BASE_API+'/events').success(success).error(error);
      },
      findA:function(success,error){
        $http.get(urls.BASE_API+'/events/a').success(success).error(error);
      },
      findU:function(success,error){
        $http.get(urls.BASE_API+'/events/u').success(success).error(error);
      },
      remove:function(data,success,error){

      },
      update:function(data,success,error){

      }
    };
   }])
  .factory('StatSv',['$http','urls',function ($http,urls) {
    return {
      all:function(success,error){
        $http.get(urls.BASE_API+'/statistic/all').success(success).error(error);
      },
      getUserStat:function(success,error){
        $http.get(urls.BASE_API+'/statistic/user').success(success).error(error);
      },
      getEventStat:function(success,error){
        $http.get(urls.BASE_API+'/statistic/event').success(success).error(error);
      }
    };
  }])